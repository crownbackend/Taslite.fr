<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizerRepository")
 */
class Organizer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $professionalStatus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sellpatent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estimate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $exploitPatent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $needFinance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $licensedPatent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $estimateOfRoyalites;

    public function getId()
    {
        return $this->id;
    }

    public function getProfessionalStatus(): ?string
    {
        return $this->professionalStatus;
    }

    public function setProfessionalStatus(string $professionalStatus): self
    {
        $this->professionalStatus = $professionalStatus;

        return $this;
    }

    public function getSellpatent(): ?string
    {
        return $this->sellpatent;
    }

    public function setSellpatent(string $sellpatent): self
    {
        $this->sellpatent = $sellpatent;

        return $this;
    }

    public function getEstimate(): ?string
    {
        return $this->estimate;
    }

    public function setEstimate(string $estimate): self
    {
        $this->estimate = $estimate;

        return $this;
    }

    public function getExploitPatent(): ?string
    {
        return $this->exploitPatent;
    }

    public function setExploitPatent(string $exploitPatent): self
    {
        $this->exploitPatent = $exploitPatent;

        return $this;
    }

    public function getNeedFinance(): ?string
    {
        return $this->needFinance;
    }

    public function setNeedFinance(string $needFinance): self
    {
        $this->needFinance = $needFinance;

        return $this;
    }

    public function getLicensedPatent(): ?string
    {
        return $this->licensedPatent;
    }

    public function setLicensedPatent(string $licensedPatent): self
    {
        $this->licensedPatent = $licensedPatent;

        return $this;
    }

    public function getEstimateOfRoyalites(): ?string
    {
        return $this->estimateOfRoyalites;
    }

    public function setEstimateOfRoyalites(string $estimateOfRoyalites): self
    {
        $this->estimateOfRoyalites = $estimateOfRoyalites;

        return $this;
    }
}
