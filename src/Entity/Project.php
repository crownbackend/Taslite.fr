<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $introduction;

    /**
     * @ORM\Column(type="text")
     */
    private $history;

    /**
     * @ORM\Column(type="text")
     */
    private $creation;

    /**
     * @ORM\Column(type="text")
     */
    private $explication;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $video;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFinish;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externalLinkLeetch;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externalLinkkickstarter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $price;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    public function setIntroduction(string $introduction): self
    {
        $this->introduction = $introduction;

        return $this;
    }

    public function getHistory(): ?string
    {
        return $this->history;
    }

    public function setHistory(string $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getCreation(): ?string
    {
        return $this->creation;
    }

    public function setCreation(string $creation): self
    {
        $this->creation = $creation;

        return $this;
    }

    public function getExplication(): ?string
    {
        return $this->explication;
    }

    public function setExplication(string $explication): self
    {
        $this->explication = $explication;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getIsFinish(): ?bool
    {
        return $this->isFinish;
    }

    public function setIsFinish(?bool $isFinish): self
    {
        $this->isFinish = $isFinish;

        return $this;
    }

    public function getExternalLinkLeetch(): ?string
    {
        return $this->externalLinkLeetch;
    }

    public function setExternalLinkLeetch(?string $externalLinkLeetch): self
    {
        $this->externalLinkLeetch = $externalLinkLeetch;

        return $this;
    }

    public function getExternalLinkkickstarter(): ?string
    {
        return $this->externalLinkkickstarter;
    }

    public function setExternalLinkkickstarter(?string $externalLinkkickstarter): self
    {
        $this->externalLinkkickstarter = $externalLinkkickstarter;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
