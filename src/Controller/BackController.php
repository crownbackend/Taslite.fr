<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackController extends Controller {

    /**
     * @Route("/admin")
     * @return Response
     */
    public function index(): Response {

        return $this->render('back/index.html.twig', [
            'title' => 'Espace Admin'
        ]);
    }

    /**
     * @Route("/admin/categorie")
     * @return Response
     */
    public function categorie(): Response {
        return $this->render('back/categorie.html.twig', [
            'title' => 'Ajouter une categorie'
        ]);
    }

}