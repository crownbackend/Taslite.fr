<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends Controller
{

    /**
     * @Route("/")
     * @return Response
     */

    public function index(): Response {

        return $this->render('index.html.twig', [
            'title' => 'Taslite.fr'
        ]);
    }

    /**
     * @Route("/project/")
     * @return Response
     */
    public function project(): Response {

        return $this->render('Front/project.html.twig', [
            'title' => 'Project'
        ]);
    }

    /**
     * @Route("/assistance-telephonique")
     * @return Response
     */
    public function assistanceTelephonique(): Response {

        return $this->render('Front/other/assistance.html.twig',
            ['title' => 'Assitance telephonique']);
    }

    /**
     * @Route("/rejoignez-nous")
     * @return Response
     */
    public function rejoignezNous(): Response {

        return $this->render('Front/other/rejoignez-nous.html.twig',
            ['title' => 'Rejoignez nous']);
    }

    /**
     * @Route("/garantie")
     * @return Response
     */
    public function garantie(): Response {

        return $this->render('Front/other/garantie.html.twig',
            ['title' => 'Garanties']);
    }

    /**
     * @Route("/press")
     * @return Response
     */
    public function press(): Response {

        return $this->render('Front/other/press.html.twig',
            ['title' => 'Press']);
    }

    /**
     * @Route("/condition-generales-de-vente")
     * @return Response
     */
    public function cgv(): Response {

        return $this->render('Front/other/cgv.html.twig',
            ['title' => 'Condition générale de vente']);
    }

    /**
     * @Route("/condition-generales-dutilisattion-du-site-internet")
     * @return Response
     */
    public function cgu(): Response {

        return $this->render('Front/other/cgu.html.twig',
            ['title' => 'Condition generales dutilisattion du site internet']);
    }

    /**
     * @Route("/actualite")
     * @return Response
     */
    public function actualite(): Response {

        return $this->render('Front/other/actualite.html.twig',
            ['title' => 'Condition générale de vente']);
    }

    /**
     * @Route("/affilation")
     * @return Response
     */
    public function affilation(): Response {

        return $this->render('Front/other/affilation.html.twig',
            ['title' => 'Affilation', 'description' => '']);
    }









}